<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
        'full_name' => $faker->name,
        'parent_id' => null,
        'post' => $faker->jobTitle,
        'employment_date' => Carbon::parse("- {$faker->numberBetween(1, 100)} days")->toDateTimeString(),
        'salary' => $faker->randomFloat(2, 1000, 10000)
    ];
});

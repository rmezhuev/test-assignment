<?php

use App\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    private $totalMax = 50000;

    const MAX_CHILDREN = 50;

    const MAX_LEVELS = 5;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::truncate();

        while ($this->totalMax > 0){
            $employee = factory(Employee::class)->create();
            $this->createEmployeeBranch($employee->id, rand(0, self::MAX_LEVELS - 1));
        }
    }

    private function createEmployeeBranch($parentId = null, $depth = 0)
    {
        if($depth <= 0 || $this->totalMax < 0){
            return;
        }

        $childrenCount = rand(1, self::MAX_CHILDREN);
        $childrenCount = $childrenCount > $this->totalMax
            ? $this->totalMax
            : $childrenCount;

        $employees = factory(Employee::class, $childrenCount)->create(['parent_id' => $parentId]);

        $depth --;
        $this->totalMax = $this->totalMax - ($childrenCount + 1);

        foreach ($employees as $employee){
            $this->createEmployeeBranch($employee->id, $depth);
        }

    }

}

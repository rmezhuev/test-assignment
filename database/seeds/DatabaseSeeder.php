<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (\DB::getDriverName() === 'sqlite') {
            \DB::statement('PRAGMA foreign_keys = OFF;');
        } else {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        $this->call(EmployeeSeeder::class);
        $this->call(UsersTableSeeder::class);

        if (\DB::getDriverName() === 'sqlite') {
            \DB::statement('PRAGMA foreign_keys = ON;');
        } else {
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

    }
}

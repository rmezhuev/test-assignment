<?php

namespace Tests\Unit;

use App\Mapping\Mappable;
use App\Mapping\MappableInterface;
use Tests\AbstractTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MapperTest extends AbstractTestCase
{
    use Mappable;

    private $map = [];

    private $keyWithoutMapping = 'notMapped';

    protected function setUp()
    {
        parent::setUp();

        for($i = 0; $i < 10; $i++){
            $this->map[self::$faker->unique()->userName] = self::$faker->unique()->userName;
        }
    }

    public function testItShouldMapDirect()
    {
        $keys = self::$faker->randomElements(array_keys($this->map), 5);
        $data = [$this->keyWithoutMapping => 'value'];

        foreach($keys as $key){
            $data[$key] = self::$faker->company;
        }

        $mapped = $this->mapDirect($data, $this->map);

        $this->assertMapping($data, $this->map, $keys, $mapped);
    }

    public function testItShouldMapReverse()
    {
        $keys = self::$faker->randomElements(array_values($this->map), 5);
        $data = [$this->keyWithoutMapping => 'value'];

        foreach($keys as $key){
            $data[$key] = self::$faker->company;
        }

        $mapped = $this->mapReverse($data, $this->map);


        $this->assertMapping($data, array_flip($this->map), $keys, $mapped);
    }

    private function assertMapping($data, $map, $keys, $mapped)
    {
        $this->assertEquals(array_values($data), array_values($mapped));

        foreach ($keys as $key){
            $this->assertArrayHasKey($map[$key], $mapped);
        }

        $this->assertArrayHasKey($this->keyWithoutMapping, $mapped);
    }

}

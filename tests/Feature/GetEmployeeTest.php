<?php


use Illuminate\Foundation\Testing\RefreshDatabase;

class GetEmployeeTest extends \Tests\AbstractTestCase
{
    use RefreshDatabase;
    use \App\Mapping\Mappable;

    public function testShouldShowEmployee()
    {
        $employee = $this->makeEmployee();

        $response = $this->actingAs(self::getUser())->get(route('employee.show', ['id' => $employee['id']]));

        $this->assertArraySubset($employee, $response->viewData('employee'));
    }

}

<?php

namespace Tests\Feature;

use App\Employee;
use App\Http\Controllers\EmployeeController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\AbstractTestCase;

class GetEmployeeListTest extends AbstractTestCase
{
    use RefreshDatabase;

    private $employees;

    protected function setUp()
    {
        parent::setUp();
        $this->employees = $this->makeEmployee([], EmployeeController::COUNT_PER_PAGE * 3);
        $this->actingAs(self::getUser());
    }

    public function testShouldGetEmployeeListFirstPage()
    {
        $response = $this->get(route('employee.manage'));

        $names = $this->getEmployeeNamesForPage(1);

        $this->assertSeeDataInResponse($response, $names);
    }

    private function assertSeeDataInResponse(TestResponse $response, array $data)
    {
        $response->assertStatus(200);

        foreach ($data as $value) {
            $response->assertSee(htmlspecialchars($value, ENT_QUOTES));
        }
    }

    private function getEmployeeNamesForPage(
        int $page,
        string $orderBy = Employee::DEFAULT_ORDER_FIELD,
        string $direction = Employee::DEFAULT_DIRECTION
    ) {
        $data = array_column($this->employees, $orderBy);

        if ($direction === 'asc') {
            sort($data);
        } else {
            rsort($data);
        }

        $data = array_slice(
            $data,
            EmployeeController::COUNT_PER_PAGE * ($page - 1),
            EmployeeController::COUNT_PER_PAGE
        );

        return $data;
    }


}

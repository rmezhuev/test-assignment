<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\AbstractTestCase;

/**
 * Class MoveEmployeesTest
 *
 * @package \Tests\Feature
 */
class MoveEmployeesTest extends AbstractTestCase
{
    use RefreshDatabase;


    public function testShouldMoveEmployees()
    {
        $oldSupervisor = $this->makeEmployee();
        $employes = $this->makeEmployee(['parent_id' => $oldSupervisor['id']], 5, ['id']);
        $newSupervidor = $this->makeEmployee();

        $this->post(route('employee.move'), [
            'old_supervisor_id' => $oldSupervisor['id'],
            'new_supervisor_id' => $newSupervidor['id']
        ]);

        foreach ($employes as $employee) {
            $this->assertDatabaseHas('employees', array_merge($employee, [ 'parent_id' => $newSupervidor['id']]));
        }
    }
}

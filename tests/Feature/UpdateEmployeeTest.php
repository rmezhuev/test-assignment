<?php

namespace Tests\Feature;

use App\Employee;
use App\Repositories\EmployeeRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\AbstractTestCase;

class UpdateEmployeeTest extends AbstractTestCase
{
    use RefreshDatabase;

    public function testShouldUpdateEmployee()
    {
        $newData = factory(Employee::class)->make()->toArray();
        $employee = $this->makeEmployee();
        $updateData = array_merge($employee, $newData);

        $response = $this->actingAs(self::getUser())->put(route('employee.update', $updateData['id']), $updateData);

        $repository = resolve(EmployeeRepository::class);

        $updatedEmployee = $repository->findOrFail($updateData['id']);

        $this->assertArraySubset($updateData, $updatedEmployee);
    }


}

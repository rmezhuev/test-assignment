<?php

namespace Tests\Feature;

use App\Http\Controllers\EmployeeController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\AbstractTestCase;

/**
 * Class GetEmployeeAjaxListTest
 *
 * @package \Tests\Feature
 */
class GetEmployeeAjaxListTest extends AbstractTestCase
{
    use RefreshDatabase;

    protected function setUp()
    {
        parent::setUp();
        $this->actingAs(self::getUser());
    }

    public function testShouldGetEmployeeList()
    {
        $searchText = 'myTarget';

        $employees = $this->generateTestData($searchText);

        $employees = $this->formatListAccordingToResponse($employees, $searchText);

        $moreToLoad = (count($employees) - EmployeeController::COUNT_PER_PAGE) > 0;

        $response = $this->get(route('ajax.employee.list', ['search' => $searchText]));

        $response->assertJson(['results' => $employees,  "pagination" => ['more' => $moreToLoad ]]);
    }


    public function testShouldGetEmployeeById()
    {
        $employees = $this->generateTestData();
        $employee  = array_shift($employees);
        $response = $this->get(route('ajax.employee.list', ['id' => $employee['id']]));

        $employee['selected'] = true;
        $expected = json_encode(['results' => [$employee],  "pagination" => ['more' => false ]]);

        $this->assertEquals($expected, $response->content());
    }


    protected function formatListAccordingToResponse($employees, $searchText)
    {
        $employees = collect($employees)->sortBy('text')->filter(
            function ($employee) use ($searchText) {
                return strpos($employee['text'], $searchText) !== false;
            })->values()->toArray();

        return $employees;
    }

    protected function generateTestData($searchText = ''): array
    {
        $employees = [];
        for ($i = 0; $i < 5; $i++) {
            $employees[] = $this->makeEmployee(['full_name' => self::$faker->unique()->name . $searchText], 1,
                ['id', 'text']);
            $employees[] = $this->makeEmployee([], 1, ['id', 'text']);
        }

        return $employees;
    }

}

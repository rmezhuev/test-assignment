<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\AbstractTestCase;

class GetEmployeeTreeTest extends AbstractTestCase
{
    use RefreshDatabase;

    private $employees;

    protected function setUp()
    {
        parent::setUp();

        $supervisor = $this->makeEmployee([], 1);
        $employees = $this->makeEmployee(['parent_id' => $supervisor['id']],2);

        foreach ($employees as $key => $employee){
            $employees[$key]['children'] = false;
        }

        $supervisor['children'] =  $employees;

        $this->employees = [$supervisor];
    }

    public function testShouldGetTopTwoLevelsOFEmployeeTree()
    {
        $response = $this->get(route('ajax.employee.tree'));
        $response->assertStatus(200);

        $response->assertJson($this->employees);
    }

    public function testShouldGetChildrenForSpecifiedNode()
    {
        $response = $this->get(route('ajax.employee.tree', ['id' => $this->employees[0]['id']]));
        $response->assertJson($this->employees[0]['children']);
    }

}

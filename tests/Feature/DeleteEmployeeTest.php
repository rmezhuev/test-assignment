<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\AbstractTestCase;

/**
 * Class DeleteEmployeeTest
 *
 * @package \Tests\Feature
 */
class DeleteEmployeeTest extends AbstractTestCase
{
    use RefreshDatabase;

    public function testShouldDeleteEmployee()
    {
        $employee = $this->makeEmployee();
        $this->actingAs(self::getUser())->delete(route('employee.delete', $employee['id']));

        $this->assertDatabaseMissing('employees', ['id' => $employee['id']]);
    }


}

<?php


use App\Mapping\Mappable;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddEmployeeTest extends \Tests\AbstractTestCase
{
    use RefreshDatabase;
    use Mappable;

    public function testShouldAddEmployee()
    {
        $this->withMiddleware();
        $employee = factory(\App\Employee::class)->make()->toArray();
        $employee = collect($employee)->except(['text','has_children'])->toArray();

        $this->actingAs(self::getUser())->post(route('employee.store', $employee));

        $employee['employment_date'] .= ' 00:00:00';
        $employee = $this->mapReverse($employee, \App\Employee::$map);

        $this->assertDatabaseHas('employees', $employee);
    }

}

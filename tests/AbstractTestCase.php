<?php

namespace Tests;

use App\Employee;
use App\User;
use Faker\Generator;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;

abstract class AbstractTestCase extends TestCase
{

    use WithoutMiddleware;

    /**
     * @var Generator
     */
    protected static $faker;


    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$faker = Faker::create();
    }

    public static function getUser()
    {
        return factory(User::class)->create();
    }

    public function makeEmployee(array $data = [], int $num = 1, array $columns = [])
    {
        $data  = factory(Employee::class, $num)->create($data)->toArray();

        if($columns){
             $data = array_map(
                 function ($employee) use($columns){
                     return  collect($employee)->only($columns)->all();
                 }
             , $data);
         }

        if(count($data) === 1){
            $data = array_shift($data);
            unset($data['has_children']);
        }

        return $data;
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EmployeeController@showTree')
    ->name('home');

Route::name('employee.')->middleware('auth')->group(function (){
    Route::get('manage', 'EmployeeController@manage')->name('manage');
    Route::post('employee', 'EmployeeController@store')->name('store');
    Route::get('employee/create', 'EmployeeController@create')->name('create');
    Route::delete('employee/{id}', 'EmployeeController@delete')->name('delete');
    Route::get('employee/{id}/edit', 'EmployeeController@edit')->name('edit');
    Route::put('employee/{id}', 'EmployeeController@update')->name('update');
    Route::get('employee/{id}', 'EmployeeController@show')->name('show');

    Route::get('move', 'EmployeeMoveController@show')->name('move.show');
    Route::post('move', 'EmployeeMoveController@move')->name('move');
});


Route::prefix('ajax')->name('ajax.')->middleware('auth')->group( function (){
    Route::get('employee/list', 'EmployeeController@getEmployeesForAjaxList')
        ->name('employee.list');

    Route::any('employee/{id}/remove-avatar', 'EmployeeController@removeAvatar')
        ->name('employee.remove.avatar');
});

Route::get('ajax/employee/tree', 'EmployeeController@getEmployeeNode')
    ->name('ajax.employee.tree');

Auth::routes();


Route::get('thumbnail/{template}','EmployeeController@getThumbnail')->name('image.thumbnail');

@extends('template')

@section('content')
    <h2 class="content-title">Assign Employees to new supervisor</h2>
    <div class="col-12">
        <form action="{{route('employee.move')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="old_supervisor">Old Supervisor</label>
                <select id="old_supervisor" name="old_supervisor_id" class="form-control select2">
                    <option></option>
                </select>
                <div class="form-group">
                    <label for="new_supervisor">New Supervisor</label>
                    <select id="new_supervisor" name="new_supervisor_id" class="form-control select2">
                        <option></option>
                    </select>
                </div>
            </div>
            <button class="btn btn-primary float-right" type="submit">Reassign</button>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        loadSelectList('{{route('ajax.employee.list')}}', '#old_supervisor', '{{old('old_supervisor_id')}}');
        loadSelectList('{{route('ajax.employee.list')}}', '#new_supervisor', '{{old('new_supervisor_id')}}');
    </script>
@endsection

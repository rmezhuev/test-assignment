<div class="col-12">
    <form action="{{$action}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{method_field($method)}}

        <div class="form-group">
            <label for="full-name">Avatar</label>
            <input class="dropify" type="file" name="avatar" data-height="200" data-default-file="@if(array_get($employee, 'avatar')){{asset('/storage/'.$employee['avatar'])}}@else{{old('avatar', asset('img/avatars/default.jpg'))}}@endif">
        </div>

        <div class="form-group">
            <label for="full-name">Full Name</label>
            <input type="text" class="form-control" name="full_name" id="full-name" placeholder="Full Name"
                   value="{{ old('full_name', array_get($employee, 'full_name'))}}">
        </div>
        <div class="form-group">
            <label for="post">Position</label>
            <input type="text" class="form-control" name="position" id="post" placeholder="Position"
                   value="{{old('position', array_get($employee, 'position'))}}">
        </div>
        <div class="form-group">
            <label for="start-date">Start Date</label>
            <input type="text" class="form-control" name="employment_date" id="start-date" placeholder="Y-m-d"
                   value="{{old('employment_date', array_get($employee, 'employment_date'))}}">
        </div>
        <div class="form-group">
            <label for="salary">Salary</label>
            <input type="number" min="0" step="0.01" placeholder="0.00" class="form-control" name="salary" id="salary"
                   value="{{old('salary', array_get($employee, 'salary'))}}">
        </div>
        <div class="form-group">
            <label for="supervisor">Supervisor</label>
            <select id="supervisor" name="supervisor_id" class="form-control select2">
                <option></option>
            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary my-3 w-25 float-right"><i class="fa fa-save pr-1"></i>Save
            </button>
        </div>
    </form>
</div>

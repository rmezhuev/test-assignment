<script>
    $('#start-date').datetimepicker({format: 'Y-m-d', timepicker: false});

    loadSelectList(
        '{{route('ajax.employee.list')}}',
        '#supervisor',
        '{{old('supervisor_id', array_get($employee, 'supervisor_id'))}}'
    );

    @if(array_get($employee,'id'))
        initFileUploader(
            '.dropify',
            '{{route('ajax.employee.remove.avatar', array_get($employee,'id'))}}',
            {'_token': '{{ csrf_token() }}'},
        );
    @else
        initFileUploader('.dropify');
    @endif

</script>

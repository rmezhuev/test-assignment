@extends('template')

@section('content')
    <h2 class="content-title">Add new employee</h2>
    @include('employee.partials.form', ['action' => route('employee.store'), 'method' => 'post'])
@endsection

@section('scripts')
    @include('employee.partials.form_scripts')
@endsection

<div class="row">
    <div class="col">
        <table class="table table-striped">
            <thead class="thead-dark">
            <tr>
                <th>Avatar</th>
                <th>Full Name @include('_partials.sort',['route' => 'employee.manage', 'field' => 'full_name'])</th>
                <th>Position @include('_partials.sort',['route' => 'employee.manage', 'field' => 'post'])</th>
                <th>Start
                    Date @include('_partials.sort',['route' => 'employee.manage', 'field' => 'employment_date'])</th>
                <th>Salary @include('_partials.sort',['route' => 'employee.manage', 'field' => 'salary'])</th>
                <th>
                    Supervisor @include('_partials.sort',['route' => 'employee.manage', 'field' => 'supervisor_name'])</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($employees as $employee)
                <tr>
                    <td><img height="45" width="45" src="@if($employee['avatar']){{route('image.thumbnail',['45x45', 'file' => '/storage/'.$employee['avatar']])}}@else{{asset('img/avatars/default-small.jpg')}}@endif"
                             alt="ava">
                    </td>
                    <td><a href="{{route('employee.show',$employee['id'])}}">{{$employee['full_name']}}</a></td>
                    <td>{{$employee['position']}}</td>
                    <td>{{$employee['employment_date']}}</td>
                    <td>{{$employee['salary']}}$</td>
                    <td>{{array_get($employee, 'supervisor_name')}}</td>
                    <td>
                        <a href="{{route('employee.edit', $employee['id'])}}" class="pr-2"><i
                                    class="fa fa-lg fa-pencil-square"></i></a>
                        <form class="d-inline-block" method="post"
                              action="{{route('employee.delete', $employee['id'])}}">
                            @csrf
                            @method('delete')
                            <a class="action-confirm" data-message="@lang('messages.delete_confirmation', ['record' => $employee['full_name']])"
                               href="javascript:void(0);"><i class="fa fa-lg fa-remove" aria-hidden="true"></i>
                            </a>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col text-center">
        {{$paginator->links()}}
    </div>
</div>

<script>
    $('.pagination a, .sort').click(function (e) {
        e.preventDefault();
        let link = $(e.target);
        if (!link.is('a'))
            link = link.parent();
        let url = link.attr('href');
        loadAjaxList(url, '#employee-list');
    });

    initConfirmationDialog('.action-confirm')
</script>

@extends('template')
@section('content')

    <div class="col-md-6 offset-6 my-3">
        <form class="form-inline my-2 float-right">
            <input id="text-search" class="form-control mr-sm-2" type="search" placeholder="Search" name="search" aria-label="Search">
            <button id="btn-search" class="btn btn-outline-success my-2" type="submit">Search</button>
        </form>
    </div>

    <div id="employee-list" class="col-12">
        @include('employee.ajax-list')
    </div>

    <script>
        $('#btn-search').click(function (e) {
            e.preventDefault();
            let url = '{{route('employee.manage')}}' + '?search=' + $('#text-search').val();
            loadAjaxList(url, '#employee-list');
        })
    </script>
@endsection

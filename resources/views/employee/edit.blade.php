@extends('template')

@section('content')
    <h2 class="content-title">Edit Employee - {{$employee['full_name']}}</h2>
    @include('employee.partials.form', ['action' => route('employee.update', $employee['id']), 'method' => 'put'])
@endsection

@section('scripts')
    @include('employee.partials.form_scripts')
@endsection

@extends('template')

@section('content')
    <h2 class="content-title"></h2>
    <div class="col-12">
        <div class="card" >
            <div class="card-header">
                <div class="row">
                    <h2 class="col-8 mx-auto">{{$employee['full_name']}}</h2>
                    <div class="col-4"><img src="@if($employee['avatar']){{asset('/storage/'.$employee['avatar'])}}@else{{asset('img/avatars/default.jpg')}}@endif"
                                            alt="" height="100" width="100" class="float-right" style="max-height: 100px; max-width: 100px">
                    </div>
                </div>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        <label class="col-6 font-weight-bold">Position</label>
                        <p class="col-6">{{$employee['position']}}</p>
                    </div>

                </li>
                <li class="list-group-item">
                    <div class="row">
                        <label class="col-6 font-weight-bold">Salary</label>
                        <p class="col-6">{{$employee['salary']}}$</p>
                    </div>

                </li>
                <li class="list-group-item">
                    <div class="row">
                        <label class="col-6 font-weight-bold">Supervisor</label>
                        <p class="col-6">{{@$employee['supervisor']['full_name']}}</p>
                    </div>
                </li>

            </ul>
        </div>
    </div>
@endsection

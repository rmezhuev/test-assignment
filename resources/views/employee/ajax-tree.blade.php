@extends('template')

@section('content')
    <div class="container">
        <h2>Employees Tree</h2>
        <div class="row">
            <div class="tree col" id="tree"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        initTree('#tree', '{{route('ajax.employee.tree',['lazy'])}}');
    </script>
@endsection



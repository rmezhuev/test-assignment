<nav class="navbar navbar-expand navbar-dark bg-primary">
    <a class="navbar-brand" href="{{route('home')}}">Test Assignment</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('home')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Staff</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('employee.manage')}}">Manage</a>
                        <a class="dropdown-item" href="{{route('employee.create')}}">Add Employee</a>
                        <a class="dropdown-item" href="{{route('employee.move')}}">Reassign Supervisor</a>
                    </div>
            </li>
        </ul>
            @guest
                <div class="nav-item"><a class="btn btn-primary" href="{{route('login')}}">Login</a></div>
            @endguest
            @auth
                <span class="navbar-text">Hello, {{auth()->user()->name}}</span>
                <form class="form-inline" action="{{route('logout')}}" method="post">
                    @csrf
                    <button class="btn btn-primary ml-2"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</button>
                </form>
            @endauth
    </div>
</nav>

@if(session('message'))
    <div class="row">
        <div class="alert alert-success alert-dismissible fade show col-12" role="alert">
            <p>{{session('message')}}</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endif

@if(request()->get('order_by') == $field)
    @if(request()->get('direction', 'asc') == 'desc' )
        <a class="sort" href="{{route($route, array_merge(request()->all(), ['order_by' => $field, 'direction' => 'asc' ]))}}"> <i class="fa fa-sort-amount-desc" aria-hidden="true"></i> </a>
    @elseif(request()->get('direction', 'asc') == 'asc' )
        <a class="sort" href="{{route($route, array_merge(request()->all(), ['order_by' => $field, 'direction' => 'desc' ]))}}"> <i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a>
    @endif
@else
    <a class="sort" href="{{route($route, array_merge(request()->all(), ['order_by' => $field, 'direction' => 'asc' ]))}}"> <i class="fa fa-sort" aria-hidden="true"></i> </a>
@endif

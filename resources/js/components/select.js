export const loadSelectList = (url, selector, selectedId = null) => {
    $.ajax({
        url,
        dataType: 'json',
        data: {id: selectedId}
    }).done(function (result)  {
        const data = result.results;
        initSelect(selector, url, data);
    }).fail(function (jqXhr, textStatus, errorThrown) {
        console.error(textStatus + ": " + errorThrown + "\n", jqXhr);
    });
};

function initSelect(selector, url, data) {
    $(selector).select2({
        ajax: {
            url,
            dataType: 'json',
            data: function (params) {
                let query = {
                    search: params.term,
                    page: params.page || 1,
                    type: 'public'
                };

                return query;
            }
        },
        data,
        placeholder: 'Please select an option',
    });
}

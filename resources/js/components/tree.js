export const initTree = (selector, url) => {
    $(selector).jstree({
        'core' : {
            'data' : {
                url,
                'data' : function (node) {
                    return { 'id' : node.id };
                }
            }
        }
    });
};

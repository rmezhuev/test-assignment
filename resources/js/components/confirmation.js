export const initConfirmationDialog = (selector) => {
    $(selector).click(function () {
        let message = '';

        if ($(this).data('message')) {
            message = $(this).data('message');
        }

        let conf = confirm(message);
        if (conf) {
            $(this).parents("form").submit();
        }
    });
    $('.reset').click(function () {
        $(this).closest('form').find('input').val('');
    })
};

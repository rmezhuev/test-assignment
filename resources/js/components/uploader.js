export const initFileUploader = (selector, url = '', data={}) => {
    let fileUpload = $(selector).dropify();

    if(url !== ''){
        fileUpload.on('dropify.beforeClear', function(event, element){
            const loaded = $('.dropify-filename-inner').html();
            const preloaded = $(selector).data('default-file').split('/').pop();
            if(loaded === preloaded){
                data.avatar = preloaded;
                $.ajax({
                    url,
                    method: 'POST',
                    data
                }).fail(function (jqXhr, textStatus, errorThrown) {
                    console.error(textStatus + ": " + errorThrown + "\n", jqXhr);
                });
            }
        });
    }
};

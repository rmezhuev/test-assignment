export const loadAjaxList = (url, selector) => {
    $.ajax(
        {
            url: url
        }
    ).done(function (data) {
        $(selector).html(data);
    }).fail(function (jqXhr, textStatus, errorThrown) {
        console.error(textStatus + ": " + errorThrown + "\n", jqXhr);
    })
};

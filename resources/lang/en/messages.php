<?php

return [
    'delete_confirmation' => 'Are you sure you want to delete :record ?',
    'update_success' => 'Information was successfully updated',
    'delete_success' => 'Selected record was successfully deleted',
    'create_success' => 'New record was successfully created',
    'move_success' => 'Employees was successfully assigned to new supervisor',
];

<?php

namespace App\Repositories;

interface EmployeeRepository
{
    public function create(array $data): int;

    public function findOrFail(int $id): array;

    public function getTopOfTree(): array;

    public function getChildrenByParentId(int $parentId): array;

    public function getEmployeeList(
        int $page = 1,
        $countPerPage = PHP_INT_MAX,
        ?string $searchText = null,
        ?array $searchFields = null,
        ?string $orderField = null,
        ?string $orderDirection = null
    ): array;

    public function update(int $id, array $data);

    public function delete(int $id);

    public function move($oldSupervisorId, $newSupervisorId);
}

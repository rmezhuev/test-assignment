<?php

namespace App\Repositories\Eloquent;

use App\Employee;
use App\Mapping\Mappable;

/**
 * Class EmployeeRepository
 *
 * @package \App\Repositories\Eloquent
 */
class EmployeeRepository implements \App\Repositories\EmployeeRepository
{
    use Mappable;

    public function create(array $data): int
    {
        $data = $this->mapReverse($data, Employee::$map);

        $employee = new Employee($data);
        $employee->save();

        return $employee->id;
    }

    public function getTopOfTree(): array
    {
        return Employee::with('children')
            ->whereNull('parent_id')
            ->with('children')
            ->get()
            ->toArray();
    }

    public function getChildrenByParentId(int $parentId): array
    {
        $parent = Employee::findOrFail($parentId);

        return $parent->children->toArray();
    }

    public function getEmployeeList(
        int $page = 1,
        $countPerPage = PHP_INT_MAX,
        ?string $searchText = null,
        ?array $searchFields = null,
        ?string $orderField = null,
        ?string $orderDirection = null
    ): array {

        $offset = (($page-1) * $countPerPage);

         $list = Employee::search($searchText ?? '', $searchFields)
            ->leftJoin('employees as supervisor', 'employees.parent_id', 'supervisor.id')
            ->take($countPerPage)
            ->offset($offset)
            ->orderBy(
                $orderField ?? Employee::DEFAULT_ORDER_FIELD,
                $orderDirection ?? Employee::DEFAULT_DIRECTION
            )
            ->get(['employees.*', 'supervisor.full_name as supervisor_name'])
            ->toArray();

         return ['results' => $list, 'total' => $this->getTotalRows($searchText, $searchFields)];
    }

    private function getTotalRows(?string $searchText = null, ?array $searchFields = null): int
    {
        return Employee::search($searchText ?? '', $searchFields)->leftJoin('employees as supervisor', 'employees.parent_id', 'supervisor.id')->count();
    }


    public function findOrFail(int $id): array
    {
        return Employee::with('supervisor')->findOrFail($id)->toArray();
    }

    public function update(int $id, array $data)
    {
        $data = $this->mapReverse($data, Employee::$map);
        Employee::findOrFail($id)->update($data);
    }

    public function delete(int $id)
    {
        Employee::findOrFail($id)->delete();
    }

    public function move($oldSupervisorId, $newSupervisorId)
    {
        Employee::where('parent_id', $oldSupervisorId)->update(['parent_id' => $newSupervisorId]);
    }

}

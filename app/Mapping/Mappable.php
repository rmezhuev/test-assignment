<?php

namespace App\Mapping;

trait Mappable
{
    public function mapReverse(array $data, array $map): array
    {
        return $this->mapp($data, array_flip($map));
    }

    public function mapDirect(array $data, array $map): array
    {
        return $this->mapp($data, $map);
    }

    private function mapp(array $data, array $map)
    {
        $mapped = [];

        foreach ($data as $key => $value){
            if(in_array($key, array_keys($map))){
                $key = $map[$key];
            }

            $mapped[$key] = $value;
        }

        return $mapped;
    }
}

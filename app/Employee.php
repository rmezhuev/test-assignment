<?php

namespace App;

use App\Mapping\Mappable;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use Mappable;

    public static $map = [
        'post' => 'position',
        'parent_id' => 'supervisor_id',
    ];

    const  DEFAULT_ORDER_FIELD = 'full_name';
    const  DEFAULT_DIRECTION = 'asc';

    protected $fillable = ['full_name', 'post', 'employment_date', 'salary', 'parent_id', 'avatar'];

    public $timestamps = false;

    protected $dates = ['employment_date'];

    protected $appends = ['text', 'has_children'];

    private static $searchable = [
        'employees.full_name',
        'employees.post',
        'employees.employment_date',
        'employees.salary',
        'supervisor.full_name',
    ];

    public static $sortable = [
        'full_name',
        'post',
        'employment_date',
        'salary',
        'supervisor_name',
    ];

    protected $casts = [
        'employment_date' => 'datetime:Y-m-d',
    ];

    public function getTextAttribute()
    {
        return "{$this->full_name} - {$this->post}";
    }

    public function getHasChildrenAttribute(): bool
    {
        return $this->children()->count();
    }

    public function supervisor()
    {
        return $this->belongsTo(Employee::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Employee::class, 'parent_id');
    }

    public static function search(string $text, ?array $searchFields = null)
    {
        $query =  self::query();

        if (!mb_strlen($text)) {
            return $query;
        }

        $searchFields = $searchFields
            ? $searchFields
            : self::$searchable;

        $query->where(function ($q) use ($text, $searchFields) {
            foreach ($searchFields as $field) {
                if (in_array($field, self::$searchable)) {
                    $q->orWhere($field, 'like', "%$text%");
                }
            }
        });

        return $query;
    }

    public function toArray()
    {
        $data = parent::toArray();

        return $this->mapDirect($data, self::$map);
    }

}

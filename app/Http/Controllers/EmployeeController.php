<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\EmployeeAjaxLists;
use App\Http\Controllers\Traits\EmployeeAvatar;
use App\Repositories\EmployeeRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class EmployeeController extends Controller
{
    use EmployeeAvatar;
    use EmployeeAjaxLists;

    const COUNT_PER_PAGE = 8;
    const AVATAR_FOLDER = 'avatars';

    /**
     * @var EmployeeRepository
     */
    private $repository;

    private $rules = [
        'full_name' => ['required', 'max:255', 'min:3'],
        'position' => ['required', 'max:255', 'min:3'],
        'supervisor_id' => ['nullable', 'exists:employees,id'],
        'employment_date' => ['nullable', 'date_format:Y-m-d'],
        'salary' => ['required', 'numeric', 'min:0', 'max: 1000000'],
        'avatar' => ['image', 'max: 2048']
    ];

    public function __construct(EmployeeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function showTree()
    {
        return view('employee.ajax-tree', compact('tree'));
    }

    public function create()
    {
        return view('employee.create')->with('employee', []);
    }

    public function delete($id)
    {
        $employee = $this->repository->findOrFail($id);

        $this->repository->delete($id);

        if ($employee['avatar']) {
            $this->deleteAvatarFile($employee['avatar']);
        }

        return redirect()->route('employee.manage')->with('message', trans('messages.delete_success'));
    }

    public function show($id)
    {
        $employee = $this->repository->findOrFail($id);

        return view('employee.show', compact('employee'));
    }


    public function store()
    {
        $this->validate(request(), $this->rules);

        $data = request()->all();

        $data['avatar'] = $this->processAvatarUpload();

        $this->repository->create($data);

        return redirect()->route('employee.manage')->with('message', trans('messages.create_success'));
    }

    public function edit($id)
    {
        $employee = $this->repository->findOrFail($id);

        return view('employee.edit', compact('employee'));
    }

    public function update($id)
    {
        $this->validate(request(), $this->rules);
        $data = request()->all();
        $data['avatar'] = $this->processAvatarUpload();

        $this->repository->update($id, $data);

        return redirect()->route('employee.manage')->with('message', trans('messages.update_success'));
    }

    public function manage()
    {

        $page = request()->get('page', 1);

        $employees = $this->repository->getEmployeeList(
            $page,
            self::COUNT_PER_PAGE,
            request('search'),
            null,
            request('order_by'),
            request('direction')
        );

        $paginator = $this->constructPaginator($page, $employees['total']);

        $employees = $employees['results'];

        if (request()->ajax()) {
            return view('employee.ajax-list', compact('employees', 'paginator', 'page'));
        }

        return view('employee.manage', compact('employees', 'paginator', 'page'));
    }

    protected function constructPaginator($page, $total): LengthAwarePaginator
    {
        $paginator = new LengthAwarePaginator([], $total, self::COUNT_PER_PAGE, $page);

        $paginator
            ->withPath(route('employee.manage'))
            ->appends(request()->except('page'));

        return $paginator;
    }


}

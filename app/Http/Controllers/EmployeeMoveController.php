<?php

namespace App\Http\Controllers;


use App\Repositories\EmployeeRepository;

class EmployeeMoveController extends Controller
{
    private $rules =  [
        'old_supervisor_id' => ['required','exists:employees,id'],
        'new_supervisor_id' => ['required', 'exists:employees,id'],
    ];

    public function show()
    {
        return view('employee.move');
    }

    public function move(EmployeeRepository $repository)
    {
        $this->validate(request(), $this->rules);

        $repository->move(
            request('old_supervisor_id'),
            request('new_supervisor_id')
        );

        return redirect()->route('employee.manage')->with('message', trans('messages.move_success'));
    }
}

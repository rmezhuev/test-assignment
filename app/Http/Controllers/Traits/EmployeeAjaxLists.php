<?php

namespace App\Http\Controllers\Traits;

use App\Repositories\EmployeeRepository;

trait EmployeeAjaxLists
{
    public function getEmployeeNode(EmployeeRepository $repository)
    {
        $id = request('id');

        $data = $id === null || $id == '#'
            ? $repository->getTopOfTree()
            : $repository->getChildrenByParentId($id);

        $this->transformEmployeeForTreeView($data);

        return response()->json($data);
    }

    public function getEmployeesForAjaxList(EmployeeRepository $repository)
    {
        if (request('id')) {
            $employee = $repository->findOrFail(request('id'));
            $employee['selected'] = true;
            $employees = [$employee];
            $moreToLoad = false;
        } else {
            list($employees, $moreToLoad) = $this->getEmployeesForSelect($repository);
        }

        $employees = $this->transformEmployeeForSelectList($employees);

        return response()->json(['results' => $employees, "pagination" => ['more' => $moreToLoad]]);
    }

    private function getEmployeesForSelect(EmployeeRepository $repository): array
    {
        $page = request('page', 1);
        $searchFields = ['employees.full_name', 'employees.post'];
        $searchText = request('search');

        $employees = $repository->getEmployeeList(
            $page,
            self::COUNT_PER_PAGE,
            $searchText,
            $searchFields
        );

        $moreToLoad = ($employees['total'] - $page * self::COUNT_PER_PAGE) > 0;
        $employees = $employees['results'];

        return [$employees, $moreToLoad];
    }


    private function transformEmployeeForTreeView(array &$data)
    {
        foreach ($data as $key => $node) {
            if (is_array(@$node['children']) && count($node['children'])) {
                $this->transformEmployeeForTreeView($data[$key]['children']);
            } else {
                $data[$key]['children'] = $data[$key]['has_children'];
            }
        }
    }

    private function transformEmployeeForSelectList($employees): array
    {
        $employees = array_map(
            function ($employee) {
                return collect($employee)->only(['id', 'text', 'selected'])->toArray();
            },
            $employees);

        return $employees;
    }

}

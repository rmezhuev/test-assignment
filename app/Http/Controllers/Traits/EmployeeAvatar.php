<?php

namespace App\Http\Controllers\Traits;

trait EmployeeAvatar
{
    public function removeAvatar($id)
    {
        if (!request()->ajax()) {
            abort(404);
        }

        $avatar = request('avatar');

        $this->repository->update($id, ['avatar' => null]);
        $this->deleteAvatarFile($avatar);

        return response('', 200);
    }

    public function getThumbnail($template)
    {
        $size = config('image.templates.' . $template);
        $location = public_path() . urldecode(request('file'));

        $img = \Image::make($location)->fit(array_first($size), array_last($size));

        $response = \Response::make($img->encode());

        $response->header('Content-Type', 'image/png');

        return $response;
    }

    protected function processAvatarUpload()
    {
        $avatar = null;

        if (request()->file('avatar')) {
            $avatar = request()->file('avatar')->store(self::AVATAR_FOLDER, 'public');
            $path = \Storage::disk('public')->path($avatar);

            ['width' => $width, 'height' => $height] = config('image.templates.100X100');
            \Image::make($path)->fit($width, $height)->save($path);
        }

        return $avatar;
    }

    protected function deleteAvatarFile($avatar): void
    {
        $avatar = explode('/', $avatar);
        $fileName = array_pop($avatar);
        \Storage::disk('public')->delete(self::AVATAR_FOLDER . DIRECTORY_SEPARATOR . $fileName);
    }
}

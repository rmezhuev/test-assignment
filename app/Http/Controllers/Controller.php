<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function processFileInRequest(Request &$request, string $key, string  $folder)
    {
        $genFileName = $this->generateFileName($request->file($key));
        $request->file($key)->move(public_path().$folder, $genFileName);
        $request = new Request(array_merge($request->except($key), [$key => $folder.$genFileName]));
    }

    protected function resizeImage(string $path, array $template)
    {
            \Image::make(public_path($path))->fit($template['width'],$template['height'])->save(public_path($path));
    }

    protected function generateFileName(UploadedFile $file): string
    {
        return uniqid('', true) . '.' . $file->getClientOriginalExtension();
    }
}
